module Main exposing (main)

import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key, pushUrl)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as D exposing (Decoder, field, int, list, map3, string)
import Json.Decode.Pipeline as DP
import Process
import RemoteData exposing (RemoteData(..), WebData)
import Task
import Time exposing (Posix, millisToPosix)
import Url
import Url.Parser exposing ((<?>))
import Url.Parser.Query


type Msg
    = SetQuery String
    | URLRequest UrlRequest
    | UrlChange Url.Url
    | GotHeroes (WebData (List Hero))
    | GetHeroes


type alias Flags =
    {}


main : Program Flags Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = URLRequest
        , onUrlChange = UrlChange
        }



-- HELPERS


parseQuery : Url.Url -> Maybe String
parseQuery url =
    url
        |> Debug.log "url"
        |> Url.Parser.parse (Url.Parser.top <?> Url.Parser.Query.string "q")
        |> Maybe.andThen identity



-- CONSTANTS


debounceDelay : Float
debounceDelay =
    100



-- MODEL


type alias Hero =
    { id : Int
    , name : String
    , username : String
    }


type alias Model =
    { query : String
    , heroes : WebData (List Hero)
    , key : Key
    }


init : Flags -> Url.Url -> Key -> ( Model, Cmd Msg )
init flags location key =
    let
        query =
            Maybe.withDefault "" (parseQuery location)
    in
    ( { query = query
      , heroes = NotAsked
      , key = key
      }
    , getheroes query
    )



-- UPDATE


nextUrl : String -> String
nextUrl query =
    if String.length query == 0 then
        "/"

    else
        "/?q=" ++ Url.percentEncode query


withDebounceOf : Float -> Msg -> Cmd Msg
withDebounceOf delay msg =
    Process.sleep delay
        |> Task.perform (\_ -> msg)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case Debug.log "changing state with Msg" msg of
        SetQuery query ->
            ( { model
                | query = query
              }
            , if query /= model.query then
                pushUrl model.key (nextUrl query)

              else
                Cmd.none
            )

        URLRequest urlRequest ->
            ( model, pushUrl model.key (nextUrl model.query) )

        UrlChange url ->
            ( model, withDebounceOf debounceDelay GetHeroes )

        GotHeroes response ->
            ( { model | heroes = response }, Cmd.none )

        GetHeroes ->
            ( model, getheroes model.query )



-- VIEW


view : Model -> Browser.Document Msg
view { query, heroes } =
    { title = ""
    , body =
        [ div [ class "app" ]
            [ viewSearchInput query
            , viewheroesWebData heroes
            ]
        ]
    }


viewSearchInput : String -> Html Msg
viewSearchInput query =
    input
        [ type_ "Search friends"
        , placeholder "Search friends..."
        , class "search-input"
        , value query
        , onInput SetQuery
        ]
        [ text query ]


viewheroesWebData : WebData (List Hero) -> Html Msg
viewheroesWebData heroesWebData =
    case heroesWebData of
        NotAsked ->
            text "Start searching your friends by typing in the bar."

        Loading ->
            text "Loading..."

        Failure error ->
            Html.div [ class "list-element list-element__error" ]
                [ Html.span [ class "error" ]
                    [ viewError (buildErrorMessage error) ]
                ]

        Success heroes ->
            Html.div []
                (List.map
                    (\hero ->
                        Html.div [ class "list-element" ]
                            [ Html.span [ class "name" ]
                                [ text hero.name ]
                            , Html.span
                                [ class "username" ]
                                [ text hero.username ]
                            ]
                    )
                    heroes
                )


buildErrorMessage : Http.Error -> String
buildErrorMessage httpError =
    case httpError of
        Http.BadUrl message ->
            message

        Http.Timeout ->
            "Server is taking too long to respond. Please try again later."

        Http.NetworkError ->
            "Unable to reach server."

        Http.BadStatus statusCode ->
            "Request failed with status code: " ++ Debug.toString statusCode

        Http.BadBody message ->
            message


viewError : String -> Html Msg
viewError errorMessage =
    text errorMessage



-- HTTP


heroesUrl : String -> String
heroesUrl query =
    "http://localhost:8000/api/heroes?q=" ++ query


getheroes : String -> Cmd Msg
getheroes query =
    Http.get
        { url = heroesUrl query
        , expect =
            heroesDecoder
                |> expectJson (RemoteData.fromResult >> GotHeroes)
        }


heroesDecoder : Decoder (List Hero)
heroesDecoder =
    field "results"
        (list
            (map3
                Hero
                (field "id" int)
                (field "name" string)
                (field "username" string)
            )
        )


expectJson : (Result Http.Error a -> msg) -> D.Decoder a -> Http.Expect msg
expectJson toMsg decoder =
    Http.expectStringResponse toMsg <|
        \response ->
            case response of
                Http.BadUrl_ url ->
                    Err (Http.BadUrl url)

                Http.Timeout_ ->
                    Err Http.Timeout

                Http.NetworkError_ ->
                    Err Http.NetworkError

                Http.BadStatus_ metadata body ->
                    Err (Http.BadBody body)

                Http.GoodStatus_ metadata body ->
                    case D.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err (Http.BadBody (D.errorToString err))
